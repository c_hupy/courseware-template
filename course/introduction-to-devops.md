---
title: Introduction to DevOps
nav_order: 1
---

Hello and welcome to the Introduction to DevOps course. 

This course will serve as an introduction to DevOps. 

## Useful Links
[About](https://git-scm.com/about)
[Documentation](https://git-scm.com/doc)
[Downloads](https://git-scm.com/downloads)
[Community](https://git-scm.com/community)

## Git Fundamentals 
![git logo](https://i.imgur.com/DJEpvCv.png)

Git is a *free* and *open source* distributed version control system designed to handle everything from small to very large projects with speed and efficiency.
